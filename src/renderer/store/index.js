import Vue from 'vue';
import Vuex from 'vuex';
import Login from './modules/login';
import Posters from './modules/posters';
import ListPosters from './modules/list-posters';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    Login,
    Posters,
    ListPosters,
  },
});

export default store;
