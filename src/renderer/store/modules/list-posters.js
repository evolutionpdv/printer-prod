import Vue from 'vue';

const ListPosters = {
  namespaced: true,
  state: {
    pagination: {
      descending: true,
      page: 1,
      rowsPerPage: 10,
      sortBy: 'product',
      totalItems: 0,
      rowsPerPageItems: [1, 2, 5, 10],
    },
    selected: [],
    items: [],
  },
  mutations: {
    setPagination(state, payload) {
      state.pagination = payload;
    },
    _setItems(state, { items, totalItems }) {
      state.items = items;
      Vue.set(state.pagination, 'totalItems', totalItems);
    },
    setSelected(state, items) {
      state.selected = items;
    },
    clearSelected(state) {
      state.selected = [];
    },
  },
  actions: {
    clearSelected(context) {
      context.commit('clearSelected');
    },
    setSelected(context, items) {
      context.commit('setSelected', items);
    },
    queryItems(context) {
      return new Promise((resolve) => {
        const {
          sortBy, descending, page, rowsPerPage,
        } = context.state.pagination;

        setTimeout(() => {
          let items = context.getters.getPostersBySize.slice();
          const totalItems = items.length;

          if (sortBy) {
            items = items.sort((a, b) => {
              const sortA = a[sortBy];
              const sortB = b[sortBy];

              if (descending) {
                if (sortA < sortB) return 1;
                if (sortA > sortB) return -1;
                return 0;
              }
              if (sortA < sortB) return -1;
              if (sortA > sortB) return 1;
              return 0;
            });
          }


          if (rowsPerPage > 0) {
            items = items.slice((page - 1) * rowsPerPage, page * rowsPerPage);
          }

          context.commit('_setItems', { items, totalItems });

          resolve();
        }, 1000);
      });
    },
  },
  getters: {
    getPostersBySize: state => (paperSize) => {
      const posters =
      state.Posters.posters.filter(poster => poster.paper.description === paperSize);
      console.log(posters);
      return posters;
    },
    loading(state) {
      return state.loading;
    },
    pagination(state) {
      return state.pagination;
    },
    items(state) {
      return state.items;
    },
  },
};


export default ListPosters;
