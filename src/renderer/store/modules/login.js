import Api from '../../api';

const Login = {
  namespaced: true,
  state: {
    toLogin: true,
    token: localStorage.getItem('access_token') || null,
    client: localStorage.getItem('client') || null,
    uid: localStorage.getItem('uid') || null,
  },
  mutations: {
    setToLogin(state, isLogged) {
      state.toLogin = isLogged;
    },
    retrieveCredentialHeaders(state, credentials) {
      state.token = credentials['access-token'];
      state.client = credentials.client;
      state.uid = credentials.uid;
    },
    destroyToken(state) {
      state.token = null;
      state.client = null;
      state.uid = null;
    },
  },
  actions: {
    auth(context, credentials) {
      return new Promise((resolve, reject) => {
        Api.Login.post(credentials)
          .then((response) => {
            localStorage.setItem('access_token', response.headers['access-token']);
            localStorage.setItem('client', response.headers.client);
            localStorage.setItem('uid', response.headers.uid);

            context.commit('retrieveCredentialHeaders', response.headers);
            context.commit('setToLogin', false);
            resolve(response);
          })
          .catch((error) => {
            alert('Tente entrar novamente.');
            localStorage.removeItem('access_token');
            localStorage.removeItem('client');
            localStorage.removeItem('uid');
            context.commit('destroyToken');
            context.commit('setToLogin', true);
            reject(error);
          });
      });
    },
  },
};

export default Login;
