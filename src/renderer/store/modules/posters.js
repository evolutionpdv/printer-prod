import Api from '../../api';

const Posters = {
  namespaced: true,
  state: {
    posters: [],
    imgPreview: null,
  },
  mutations: {
    setPosters(state, posters) {
      state.posters = posters.impressions;
    },
    updatePosters(state, posters) {
      state.posters = posters.impressions;
    },
  },
  actions: {
    update(context, poster) {
      Api.Posters.update(poster.id, poster)
        .then((response) => {
          context.commit('updatePosters', response.data.posters);
        })
        .catch(alert('Tente atualizar novamente.'));
    },
    getPosters(context) {
      Api.Posters.get()
        .then((response) => {
          context.commit('setPosters', response.data);
        });
    },
  },
  getters: {
    getPostersBySize: state => (paperSize) => {
      alert(`O Papel é ${paperSize}`);
      const postersBySize = state.posters.filter(poster => poster.paper === paperSize);
      console.log(`A lista é ${postersBySize}`);
      return postersBySize;
    },
  },
};

export default Posters;
