import Http from './http';
// import { serialize } from 'uri-js';

export default {
  post(credentials) {
    return Http.post('/auth/sign_in', credentials);
  },
};
