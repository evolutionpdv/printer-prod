import fs from 'fs';
import rp from 'request-promise';
import AppSettings from '../../models/app-settings';
import RandomGenerator from '../lib/random-generator';
// import HttpPdf from './http-pdf';
// import { serialize } from 'uri-js';

export default {
  downloadSavePdf(id, path) {
    const url1 = `${AppSettings.API_ENDPOINT}/impressions/${id}.pdf`;
    const credentialHeaders = {
      'access-token': localStorage.getItem('access_token'),
      client: localStorage.getItem('client'),
      uid: localStorage.getItem('uid'),
    };
    const options = {
      url: url1,
      headers: credentialHeaders,
      encoding: 'binary',
    };
    return new Promise((resolve, reject) => {
      rp(options)
        .then((body) => {
          const pathName = `${path}/downloadedPdf-${RandomGenerator.generateRandomInt()}.pdf`;
          const writeStream = fs.createWriteStream(pathName);
          writeStream.write(body, 'binary');
          writeStream.end();
          writeStream.on('finish', () => {
            resolve(pathName);
          });
          writeStream.on('error', () => {
            reject();
          });
        });
    });
  },
};
