import Http from './http';
// import { serialize } from 'uri-js';

export default {
  get() {
    return Http.get('/impressions');
  },
  update(id, options) {
    return Http.patch(`/impressions/${id}`, options);
  },
};
