import axios from 'axios';
// import AppSettings from '../../models/app-settings.js';

const httpPdf = axios.create({
  baseURL: 'https://evolutionpdv.com.br',
  timeout: 10000,
  headers: {
    'access-token': localStorage.getItem('access_token'),
    client: localStorage.getItem('client'),
    uid: localStorage.getItem('uid'),
  },
});

export default httpPdf;
