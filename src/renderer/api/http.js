import axios from 'axios';
import AppSettings from '../../models/app-settings.js';

const http = axios.create({
  baseURL: AppSettings.API_ENDPOINT,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    'access-token': localStorage.getItem('access_token'),
    client: localStorage.getItem('client'),
    uid: localStorage.getItem('uid'),
  },
});

export default http;
