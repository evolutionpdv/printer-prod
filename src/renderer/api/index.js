import Posters from './posters';
import Login from './login';
import Downloader from './downloader';

export default {
  Posters,
  Login,
  Downloader,
};
