// import electron from 'electron';
// import PdfCreator from './pdf-creator';

// const tmpPath = electron.remote.app.getPath('temp');

// class PdfMerger {
//   constructor(
//     pdfFiles,
//     outputFile,
//   ) {
//     this.pdfFiles = pdfFiles;
//     this.outPutFile = outputFile;
//   }

//   mergeA5toA4() {
//     // Maximum plottable height (297 mm) - Conversion to point
//     const maxPageHeight = (297 / 0.3528);

//     console.log(this.pdfFiles);
//     // Read files and determine width/length of plot
//     const File1 = this.pdfFiles[0] || null;
//     const File2 = this.pdfFiles[1] || null;
//     let fileOne;
//     let fileTwo;
//     if (File1) {
//       console.log(File1.filePath);
//       fileOne = new HummusRecipe(File1.filePath, `${tmpPath}/hummus1.pdf`);
//     } else {
//       fileOne = PdfCreator.createBlankA4();
//     }
//     if (File2) {
//       console.log(File2.filePath);
//       fileTwo = new HummusRecipe(File2.filePath, `${tmpPath}/hummus2.pdf`);
//     } else {
//       fileTwo = PdfCreator.createBlankA4();
//     }
//     const width = Math.max(fileOne.pageInfo(1).width, fileTwo.pageInfo(1).width) + 30;
//     console.log(fileOne);
//     console.log(fileTwo);
//     // Create new pdf file
//     const pdfDoc = new HummusRecipe('new', this.outPutFile, {
//       version: 1.6,
//       author: 'EvoPdv',
//       title: 'Cartaz A5',
//       subject: 'Dois cartazes a5 em uma página A4.',
//     });

//     // Get height of first pdf to generate offsett
//     const heightOne = fileOne.pageInfo(1).height;
//     // Overlay PDFs to new pdf with offset and ~ 5mm margin
//     pdfDoc
//       .createPage(width, maxPageHeight)
//       .overlay(fileOne, 15, 15)
//       .overlay(fileTwo, 15, heightOne + 15)
//       .endPage()
//       .endPDF();
//     // fs.delete('hummus1.pdf');
//     // fs.delete('hummus2.pdf')/
//     return pdfDoc;
//   }

//   mergeA6toA4() {
//     // Maximum plottable height (297 mm) - Conversion to point
//     const maxPageHeight = (297 / 0.3528);

//     // Read files and determine width/length of plot
//     const File1 = this.pdfFiles[0] || null;
//     const File2 = this.pdfFiles[1] || null;
//     const File3 = this.pdfFiles[2] || null;
//     const File4 = this.pdfFiles[3] || null;
//     let fileOne;
//     let fileTwo;
//     let fileThree;
//     let fileFour;
//     if (File1) {
//       fileOne = new HummusRecipe(File1, `${tmpPath}/hummus1.pdf`);
//     } else {
//       fileOne = PdfCreator.createBlankA4();
//     }
//     if (File2) {
//       fileTwo = new HummusRecipe(File2, `${tmpPath}/hummus2.pdf`);
//     } else {
//       fileTwo = PdfCreator.createBlankA4();
//     }
//     if (File3) {
//       fileThree = new HummusRecipe(File3, `${tmpPath}/hummus3.pdf`);
//     } else {
//       fileThree = PdfCreator.createBlankA4();
//     }
//     if (File4) {
//       fileFour = new HummusRecipe(File4, `${tmpPath}/hummus4.pdf`);
//     } else {
//       fileFour = PdfCreator.createBlankA4();
//     }
//     const width = Math.max(
//       fileOne.pageInfo(1).width,
//       fileTwo.pageInfo(1).width,
//       fileThree.pageInfo(1).width,
//       fileFour.pageInfo(1).width,
//     ) + 30;

//     // Create new pdf file
//     const pdfDoc = new HummusRecipe('new', this.outPutFile, {
//       version: 1.6,
//       author: 'EvoPdv',
//       title: 'Cartaz A5',
//       subject: 'Quatro cartazes a6 em uma página A4.',
//     });

//     // Get height of first pdf to generate offsett
//     const heightOne = fileOne.pageInfo(1).height;
//     // Overlay PDFs to new pdf with offset and ~ 5mm margin
//     pdfDoc
//       .createPage(width, maxPageHeight)
//       .overlay(fileOne, 15, 15)
//       .overlay(fileTwo, 15, heightOne + 15)
//       .overlay(fileThree, 15, heightOne + 15)
//       .overlay(fileFour, 15, heightOne)
//       .endPage()
//       .endPDF();
//     // fs.delete('hummus1.pdf');
//     // fs.delete('hummus2.pdf')/
//     return pdfDoc;
//   }
// }

// export default PdfMerger;
