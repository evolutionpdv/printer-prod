import command from 'child_process';

const JavaMultivalent = {
  promiseMerge(FilePaths, outFile) {
    const merge = this.commandMerge(FilePaths);
    command.exec(merge, (err, stdout) => {
      if (err) reject(err);
      this.outfile = outFile;
      console.log(`merge chegou no resolve\n${outFile}`);
      return outFile;
    });
  },
  promiseImposeA5(fileName) {
    const nup = this.commandImposeA5(fileName);
    this.fileA5Name = fileName;
    command.exec(nup, (err, stdout) => {
      if (err) reject(err);
      const imposedFilePath = `${this.fileNameA5Name}-m-up.pdf`;
      console.log(`nup chegou no resolve\n${stdout} ${imposedFilePath}`);
      return imposedFilePath;
    });
  },
  promiseImposeA6(fileName) {
    return new Promise((resolve, reject) => {
      const nup = this.commandImposeA6(fileName);
      command.exec(nup, (err, stdout) => {
        if (err) reject(err);
        const imposedFilePath = `${fileName}-m-up.pdf`;
        console.log(`nup chegou no resolve\n${stdout}`);
        resolve(imposedFilePath);
      });
    });
  },
  commandImposeA6(fileName) {
    return `java -classpath ${process.resourcesPath}/multivalent.jar tool.pdf.Impose -nup 4 -margin 1cm -sep 1 ${fileName}-m.pdf`;
  },
  commandImposeA5(fileName) {
    return `java -classpath ${process.resourcesPath}/multivalent.jar tool.pdf.Impose -nup 2 -margin 1cm -sep 1 ${fileName}-m.pdf`;
  },
  commandMerge(filePaths) {
    return `java -classpath ${process.resourcesPath}/multivalent.jar tool.pdf.Merge ${filePaths}`;
  },
};

export default JavaMultivalent;
