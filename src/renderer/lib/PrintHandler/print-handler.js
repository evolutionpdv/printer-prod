import printer from 'printer';
import fs from 'fs';
import imagemagick from 'imagemagick-native';
import PdfHandler from './pdf-handler';

class PrintHandler {
  constructor(
    pdfsToPrint,
    printerName,
    paperSize,
  ) {
    this.pdfsToPrint = pdfsToPrint;
    this.printerName = printerName;
    this.paperSize = paperSize;
  }

  print() {
    return new Promise((resolve, reject) => {
      const pdfHandler = new PdfHandler(this.pdfsToPrint, this.paperSize);
      const pdfToPrint = pdfHandler.pdf();
      console.log(pdfToPrint);
      this.pdfToPrint = pdfToPrint;
      this.sendToPrint()
        .then((jobId) => {
          alert(`arquivo ${this.pdfToPrint} enviado para ${jobId}`);
          resolve(jobId);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }

  async sendToPrint() {
    // use: node printFile.js [filePath printerName]
    const fileName = this.pdfToPrint;
    console.log(`platform:, ${process.platform}`);
    console.log(`try to print file: ${fileName}`);
    let jobId;
    if (process.platform === 'win32') {
      jobId = await this.printToWindows();
    } else {
      jobId = await this.printToLinux();
    }
    return jobId;
  }

  printToLinux() {
    const fileName = this.pdfToPrint;
    return new Promise((resolve, reject) => {
      printer.printDirect({
        data: fs.readFileSync(fileName),
        printer: this.printerName,
        success: jobID => resolve(jobID),
        error: err => reject(err),
      });
    });
  }
  printToWindows() {
    const fileName = this.pdfToPrint;
    const data = fs.readFileSync(fileName);
    return new Promise((resolve, reject) => {
      imagemagick.convert({
        srcData: data,
        srcFormat: 'PDF',
        format: 'EMF',
      }, (err, buffer) => {
        if (err) {
          reject(err);
        }
        printer.printDirect({
          data: buffer,
          type: 'EMF',
          success: jobId => resolve(jobId),
          error: err => reject(err),
        });
      });
    });
  }
}
export default PrintHandler;
