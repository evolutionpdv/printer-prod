import PdfMergerController from './pdf-merger-controller';

class PdfHandler {
  constructor(
    pdfsToPrint,
    paperSize,
  ) {
    this.pdfsToPrint = pdfsToPrint;
    this.paperSize = paperSize;
  }
  pdf() {
    let filePath;
    switch (this.paperSize) {
      case 'A6':
        filePath = PdfMergerController.mergeA6ToA4(this.pdfsToPrint);
        break;
      case 'A5':
        filePath = PdfMergerController.mergeA5ToA4(this.pdfsToPrint);
        // this.promiseA5 = new Promise((resolve) => {
        //   PdfMergerController.mergeA5ToA4(this.pdfsToPrint).then((result) => {
        //     filePath = result;
        //     console.log(`${filePath} veio assim`);
        //     resolve(filePath);
        //   });
        // });
        break;
      case 'A3':
        filePath = PdfMergerController.mergeA4ToA3(this.pdfsToPrint);
        break;
      default:
        filePath = this.pdfsToPrint.filePath;
    }
    return filePath;
  }
}

export default PdfHandler;
