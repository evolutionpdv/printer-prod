import electron from 'electron';
import shellescape from 'shell-escape';
import command from 'child_process';
import RandomGenerator from '../random-generator';
import JavaMultivalent from '../PdfMerger/java-multivalent';

const tmpPath = electron.remote.app.getPath('temp');
// const nodePath = process.execPath;
const PdfMergerController = {
  async mergeA5ToA4(files) {
    let FilePaths = files.map(file => file.filePath);
    const firstFileName = files[0].filePath.replace('.pdf', '');
    FilePaths = JSON.stringify(FilePaths)
      .replace('[', '')
      .replace(']', '')
      .replace(',', ' ')
      .split('"')
      .join('');
    console.log(FilePaths);
    // const cmd = ['pdfjam', FilePaths, '--batch', '--nup', '2x1',
    //  '--landscape', '--outfile', tmpPdf];
    const fileMerge = await JavaMultivalent.promiseMerge(FilePaths, firstFileName);
    await fileMerge.promiseImposeA5(fileMerge.outfile);
  },
  mergeA6ToA4(files) {
    const tmpPdf = `${tmpPath}/tmpPdf-${RandomGenerator.generateRandomInt()}.pdf`;
    let FilePaths = files.map(file => file.filePath);
    FilePaths = JSON.stringify(FilePaths).replace('[', '').replace(']', '')
      .split('"')
      .join('');
    const cmd = ['pdfjam', FilePaths, '--batch', '--nup', '2x2', '--outfile', tmpPdf];
    command.exec(shellescape(cmd));
    return tmpPdf;
  },
  mergeA4ToA3(files) {
    const File1 = files.filePath || '';
    const tmpPdf = `${tmpPath}/-${RandomGenerator.generateRandomInt}.pdf`;
    const cmd = ['pdfjam', '--outfile', tmpPdf, '--paper', 'a3paper', File1];
    command.exec(shellescape(cmd));
    return tmpPdf;
  },
};

export default PdfMergerController;
