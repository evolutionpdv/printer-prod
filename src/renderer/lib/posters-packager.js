import PostersPackage from '../../models/posters-package';

const PostersPackager = {
  postersPackage(posters) {
    const paper = posters[0].paper;
    let packages = [];
    switch (paper) {
      case 'A3':
        packages = this.packageA3(posters);
        break;
      case 'A4':
        packages = this.packageA4(posters);
        break;
      case 'A5':
        packages = this.packageA5(posters);
        break;
      case 'A6':
        packages = this.packageA6(posters);
        break;
      default:
        packages = this.packageA4(posters);
    }
    return packages;
  },
  packageA3(posters) {
    const packages = [];
    posters.forEach((poster) => {
      const posterPackage = new PostersPackage(poster);
      packages.push(posterPackage);
    });
    return packages;
  },
  packageA4(posters) {
    const packages = [];
    posters.forEach((poster) => {
      const posterPackage = new PostersPackage(poster);
      packages.push(posterPackage);
    });
    return packages;
  },
  packageA5(posters) {
    const packages = [];
    const chunk = 2;
    let i = 2;
    let j = 2;
    let temparray = 2;
    for (i = 0, j = posters.length; i < j; i += chunk) {
      temparray = posters.slice(i, i + chunk);
      console.log(temparray);
      const posterPackage = new PostersPackage(temparray);
      packages.push(posterPackage);
    }
    return packages;
  },
  packageA6(posters) {
    const packages = [];
    const chunk = 4;
    let i = 4;
    let j = 4;
    let temparray = 4;
    for (i = 0, j = posters.length; i < j; i += chunk) {
      let posterPackage;
      if (posters instanceof Array) {
        temparray = posters.slice(i, i + chunk);
        posterPackage = new PostersPackage(temparray);
        packages.push(posterPackage);
      }
      else {
        posterPackage = new PostersPackage(posters);
        packages.push(posterPackage);
      }
    }
    return packages;
  },
};

export default PostersPackager;
