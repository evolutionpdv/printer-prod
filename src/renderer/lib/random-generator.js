const RandomGenerator = {
  generateRandomInt() {
    const Min = 1;
    const Max = 9999999999999;
    const Random = Math.floor(Math.random() * (Max - Min + 1)) + Min;
    return Random;
  },
};

export default RandomGenerator;
