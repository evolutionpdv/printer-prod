import shellescape from 'shell-escape';
const SystemCommander = {
  sendCommand(cmd) {
    const escaped = shellescape(cmd);
    console.log(escaped);
  },
};

export default SystemCommander;
