import Vue from 'vue';
import Router from 'vue-router';

import Posters from '@/components/Posters/Posters';
import Auth from '@/components/Login/Auth';
import Printing from '@/components/Posters/Printing';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'posters',
      component: Posters,
    },
    {
      path: '/auth',
      name: 'auth',
      component: Auth,
    },
    {
      path: '/printing',
      name: 'printing',
      component: Printing,
    },
  ],
});

router.beforeEach((to, from, next) => {
  const publicPages = ['/auth'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('access_token');
  if (authRequired && !loggedIn) {
    return next('/auth');
  }
  return next();
});

export default router;
