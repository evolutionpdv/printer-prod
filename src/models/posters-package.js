import electron from 'electron';
import PrintHandler from '../renderer/lib/PrintHandler/print-handler';
import Api from '../renderer/api';

const tmpPath = electron.remote.app.getPath('temp');

class PostersPackage {
  constructor(posters) {
    this.States = Object.freeze({
      waitingToDownload: 1, downloading: 2, waitingToPrint: 3, printing: 4, printed: 5,
    });
    this.posters = posters;
    this.state = this.States.waitingToDownload;
  }

  get filePath() {
    return this.filePath;
  }

  set filePath(value) {
    this.filePath = value;
  }

  get multiplePoster() {
    let multiple = false;
    if (this.posters instanceof Array) multiple = true;
    return multiple;
  }

  download() {
    let download;
    if (this.multiplePoster === false) {
      download = this.downloadSingle();
    }
    else {
      download = this.downloadMultiple();
    }
    return download;
  }

  downloadSingle() {
    return new Promise((resolve) => {
      this.state = this.States.downloading;
      Api.Downloader.downloadSavePdf(this.posters.id, tmpPath)
        .then((response) => {
          this.posters.filePath = response;
          resolve();
        })
        .catch(() => {
          alert('Erro ao receber informações do servidor tente novamente');
        });
      this.state = this.States.waitingToPrint;
    });
  }

  downloadMultiple() {
    return new Promise((resolve) => {
      this.state = this.States.downloading;
      let i = 0;
      this.posters.forEach((poster, index, array) => {
        console.log(`tentou primeiro ${index} ${poster.id}`);
        Api.Downloader.downloadSavePdf(poster.id, tmpPath)
          .then((response) => {
            poster.filePath = response;
            i += 1;
            console.log(`cartazes no Package ${JSON.stringify(this.posters)}`);
            console.log(`index= ${index} array.length = ${array.length} i = ${i}`);
            if (array.length === i) resolve();
          })
          .catch(() => {
            alert('Erro ao receber informações do servidor tente novamente');
          });
      });
      this.state = this.States.waitingToPrint;
    });
  }

  print(printer) {
    let paperSize;
    if (this.multiplePoster === false) {
      paperSize = this.posters.paper;
    }
    else {
      paperSize = this.posters[0].paper;
    }
    const printHandler = new PrintHandler(this.posters, printer, paperSize);
    this.state = this.States.printing;
    return printHandler.print();
  }

  printDoneHandling() {
    this.posters.forEach((poster) => {
      Promise((resolve) => {
        poster.sendPrintedState().then(() => {
          resolve();
        });
      });
    });
  }
}

export default PostersPackage;
