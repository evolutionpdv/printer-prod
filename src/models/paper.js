export class Paper {
  constructor(
    id: String,
    description: String,
    name: String,
  ) {
    this.id = id;
    this.description = description;
    this.name = name;
  }
}

export default Paper;
