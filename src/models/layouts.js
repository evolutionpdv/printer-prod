export class Layout {
  constructor(
    id: String,
    name: String,
    description: String,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
  }
}

export default Layout;
