export class User {
  constructor(
    name: String,
    login: String,
    id: String,
  ) {
    this.name = name;
    this.login = login;
    this.id = id;
  }
}

export default User;
