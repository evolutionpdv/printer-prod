import Api from '../renderer/api';

class Poster {
  constructor(
    id,
    image,
    layout,
    paper,
    user,
    state,
  ) {
    this.id = id;
    this.image = image;
    this.layout = layout;
    this.paper = paper;
    this.user = user;
    this.state = state;
  }

  get selected() {
    return this.selected;
  }

  set selected(value) {
    this.selected = value;
  }

  get state() {
    return this.state;
  }

  set state(value) {
    this.state = value;
  }

  get filePath() {
    return this.filePath;
  }

  set filePath(value) {
    this.filePath = value;
  }

  sendPrintedState() {
    this.state = 5;
    Api.Poster.update(this.id, this).then(response => response);
  }
}

export default Poster;
